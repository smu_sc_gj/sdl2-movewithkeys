/*
  Moving the player with keys, crude, key event based.   
  - First attempt, move things within the key event. 
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

#if defined(_WIN32) || defined(_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;

int main( int argc, char* args[] )
{
    // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    
    // Player Image source and target. 
    SDL_Texture* playerTexture = nullptr;
    const int PLAYER_SPRITE_WIDTH = 64;
    const int PLAYER_SPRITE_HEIGHT = 64;
    SDL_Rect standRight; // standing sprite
    SDL_Rect targetRectangle;
    
    // Window control 
    SDL_Event event;
    bool quit = false;
    float horizontalInput = 0.0f;
    float verticalInput = 0.0f;

    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
      
    /**********************************
     *    Setup background sprite     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");

    if(temp == nullptr)
    {
        printf("Background image not found!");
    }    

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;


    /**********************************
     *    Setup mech sprite     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/walker1.png");

    if(temp == nullptr)
    {
        printf("Walker image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    targetRectangle.x = 250;
    targetRectangle.y = 250;
    targetRectangle.w = PLAYER_SPRITE_WIDTH;
    targetRectangle.h = PLAYER_SPRITE_HEIGHT;

    standRight.x = 2*PLAYER_SPRITE_WIDTH;
    standRight.y = 2*PLAYER_SPRITE_HEIGHT;
    standRight.w = PLAYER_SPRITE_WIDTH;
    standRight.h = PLAYER_SPRITE_HEIGHT;

    // Game loop
    while(!quit) // while quit is not true
    { 
        // Handle events 

        if( SDL_PollEvent( &event ))  // tes for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;

                    case SDLK_UP:
                        targetRectangle.y += -1.0f; // move up. 
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    
                    case SDLK_UP:
                        // Nothing to do here. 
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        // Render (draw stuff)

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

        SDL_RenderCopy(gameRenderer, playerTexture, &standRight, &targetRectangle);

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);

    }

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;   
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resorces etc.
    SDL_Quit();

    return 0;
}
